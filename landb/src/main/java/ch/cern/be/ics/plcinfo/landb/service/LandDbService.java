package ch.cern.be.ics.plcinfo.landb.service;

import NetworkDataTypes.Auth;
import NetworkDataTypes.DeviceInfo;
import NetworkDataTypes.Location;
import NetworkDataTypes.Person;
import NetworkService.NetworkServiceInterface;
import NetworkService.SOAPNetworkService;
import NetworkService.SOAPNetworkServiceLocator;
import ch.cern.be.ics.plcinfo.commons.Credentials;
import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.landb.dto.LanDbInfo;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

@Named
public class LandDbService {

    private final PLCInfoLogger plcInfoLogger;
    private NetworkServiceInterface soap;
    private Auth myauth;

    @Inject
    private LandDbService(PLCInfoLogger plcInfoLogger) {
        this.plcInfoLogger = plcInfoLogger;
        try {
            SOAPNetworkService nss = new SOAPNetworkServiceLocator();
            soap = nss.getNetworkServicePort();
            String credentials = soap.getAuthToken(Credentials.login, Credentials.pass2, "NICE");
            myauth = new Auth(credentials);
        } catch (ServiceException | RemoteException e) {
            this.plcInfoLogger.severe(e.getMessage());
        }
    }

    public LanDbInfo getDeviceInfo(String plcName) {
        try {
            DeviceInfo device = soap.getDeviceInfo(myauth, plcName);
            //FIXME: outlet info missing
            return new LanDbInfo(device.getManufacturer(), device.getModel(), device.getGenericType(),
                    stringify(device.getResponsiblePerson()), stringify(device.getUserPerson()),
                    stringify(device.getLocation()), device.getTag(), device.getDescription(), "outlet", device.getInterfaces()[0].getOutlet().getID());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return new LanDbInfo();
    }

    private String stringify(Person person) {
        return String.format("%s %s (%s-%s) - Phone: %s",
                person.getFirstName(), person.getName(), person.getDepartment(), person.getGroup(), person.getPhone());
    }

    private String stringify(Location location) {
        return String.format("%s %s-%s", location.getBuilding(), location.getFloor(), location.getRoom());
    }
}




