package ch.cern.be.ics.plcinfo.moon.domain.configuration.plc;

import javax.inject.Named;

@Named("SCHNEIDER_CONFIGURATION")
public class SchneiderDatapointConfiguration extends BasePlcDatapointConfiguration {
}
