package ch.cern.be.ics.plcinfo.moon.dto;

public class MoonDatapointMetadata {
    private final MoonDpConfig moonDpConfig;
    private final String okValue;

    public MoonDatapointMetadata(MoonDpConfig moonDpConfig, String okValue) {
        this.moonDpConfig = moonDpConfig;
        this.okValue = okValue;
    }

    public MoonDpConfig getMoonDpConfig() {
        return moonDpConfig;
    }

    public String getOkValue() {
        return okValue;
    }
}
