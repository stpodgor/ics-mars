package ch.cern.be.ics.plcinfo.moon.domain.provider;

import ch.cern.be.ics.plcinfo.moon.domain.configuration.GenericDatapointConfiguration;
import ch.cern.be.ics.plcinfo.moon.dto.*;
import ch.cern.be.ics.plcinfo.moon.service.ColourAssigner;
import ch.cern.be.ics.plcinfo.moon.service.MoonWebserviceProxy;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class GenericMoonInfoProvider implements MoonInfoProvider {
    private final MoonWebserviceProxy moonWebserviceProxy;
    private final ColourAssigner colourAssigner;
    private final Supplier<String> datapointTypeProvider;
    private final Supplier<String> dataValidityProvider;
    private final BiFunction<String, String, Optional<String>> datapointNameProvider;
    private final Function<String, GenericDatapointConfiguration> configurationProvider;

    GenericMoonInfoProvider(MoonWebserviceProxy moonWebserviceProxy,
                            ColourAssigner colourAssigner,
                            Supplier<String> datapointTypeProvider,
                            Supplier<String> dataValidityProvider,
                            BiFunction<String, String, Optional<String>> datapointNameProvider,
                            Function<String, GenericDatapointConfiguration> configurationProvider) {
        this.moonWebserviceProxy = moonWebserviceProxy;
        this.colourAssigner = colourAssigner;
        this.datapointTypeProvider = datapointTypeProvider;
        this.dataValidityProvider = dataValidityProvider;
        this.datapointNameProvider = datapointNameProvider;
        this.configurationProvider = configurationProvider;
    }

    @Override
    public Optional<MoonDeviceInformation> getDeviceInfo(String name, String manufacturer) {
        Optional<String> datapointName = datapointNameProvider.apply(name, datapointTypeProvider.get());
        return datapointName.map(dpName -> getMoonDeviceData(name, dpName, manufacturer));
    }

    private MoonDeviceInformation getMoonDeviceData(String name, String dataPointName, String manufacturer) {
        GenericDatapointConfiguration configuration = configurationProvider.apply(manufacturer);
        List<MoonDpConfig> infoDpes = includePrefix(configuration.getInfoMetadata(), dataPointName);
        List<MoonDpConfig> metricsDpes = readPlcMetricsDpes(dataPointName);
        return getMoonDeviceData(
                name,
                dataPointName,
                infoDpes,
                metricsDpes,
                getMoonWebserviceProxy()::getStateForPlc);
    }


    private MoonDeviceInformation getMoonDeviceData(String name, String dpName, List<MoonDpConfig> infoDpes, List<MoonDpConfig> metricsDpes, Function<String, MoonColouredState> getState) {
        MoonMetricsInformation metrics = new MoonMetricsInformation(includeStatusColour(getMoonWebserviceProxy().getDatapointsData(metricsDpes), dpName));
        MoonInformation moonInformation = new MoonInformation(getMoonWebserviceProxy().getDatapointsData(infoDpes));
        MoonColouredState state = getState.apply(name);
        return new MoonDeviceInformation(moonInformation, metrics, state);
    }

    private List<MoonColourDp> includeStatusColour(List<MoonDp> moonDps, String dpName) {
        Boolean dataValid = isDataValid(dpName);
        return moonDps.stream()
                .map(datapoint -> new MoonColourDp(datapoint.getMoonDpConfig(), datapoint.getValue(), getColour(datapoint.getMoonDpConfig().getDp(), dataValid)))
                .collect(Collectors.toList());
    }

    private Boolean isDataValid(String dpName) {
        String datapointName = dpName + dataValidityProvider.get();
        String status = moonWebserviceProxy.getDatapointData(datapointName)
                .orElse("false");
        if (StringUtils.isNumeric(status)) {
            return Integer.valueOf(status) == 1;
        } else {
            return Boolean.valueOf(status);
        }
    }

    private StatusColourEnum getColour(String dp, Boolean dataValid) {
        Boolean alarmActive = Boolean.valueOf(moonWebserviceProxy.getDatapointData(dp + ":_alert_hdl.._active")
                .orElse("false"));
        Integer alarmState = moonWebserviceProxy.getDatapointData(dp + ":_alert_hdl.._act_state")
                .map(Integer::valueOf)
                .orElse(-1);
        return colourAssigner.getColour(alarmState, dataValid, alarmActive);
    }

    private MoonWebserviceProxy getMoonWebserviceProxy() {
        return moonWebserviceProxy;
    }

    private List<MoonDpConfig> includePrefix(List<MoonDpConfig> data, String prefix) {
        return data.stream()
                .map(datapointConfig -> new MoonDpConfig(prefix + datapointConfig.getDp(), datapointConfig.getDescription()))
                .collect(Collectors.toList());
    }

    private List<MoonDpConfig> readPlcMetricsDpes(String dpName) {
        return getMoonWebserviceProxy().getDatapointDataList(dpName + ":_alert_hdl.._dp_list")
                .orElse(Collections.emptyList())
                .stream()
                .map(metricName -> getMoonWebserviceProxy().getMatchingDpInfo(datapointTypeProvider.get(), metricName, datapointInfo -> datapointInfo.getDpName().equalsIgnoreCase(metricName)))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(metricDpe -> new MoonDpConfig(metricDpe.getDpName(), metricDpe.getDescription()))
                .collect(Collectors.toList());
    }
}
