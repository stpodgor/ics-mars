package ch.cern.be.ics.plcinfo.moon.domain.provider;

import ch.cern.be.ics.plcinfo.moon.domain.configuration.GenericDatapointConfiguration;
import ch.cern.be.ics.plcinfo.moon.dto.MoonDeviceInformation;

import java.util.Map;
import java.util.Optional;

public interface MoonInfoProvider {
    default GenericDatapointConfiguration getConfiguration(String key, Map<String, GenericDatapointConfiguration> configurations) {
        return configurations.get(key.toUpperCase() + "_CONFIGURATION");
    }

    default Optional<MoonDeviceInformation> getDeviceInfo(String name, String manufacturer) {
        return getConcreteProvider().getDeviceInfo(name, manufacturer);
    }

    default MoonInfoProvider getConcreteProvider() {
        return this;
    }
}
