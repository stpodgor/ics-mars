package ch.cern.be.ics.plcinfo.moon.domain.provider;


import ch.cern.be.ics.plcinfo.moon.domain.configuration.GenericDatapointConfiguration;
import ch.cern.be.ics.plcinfo.moon.service.ColourAssigner;
import ch.cern.be.ics.plcinfo.moon.service.MoonWebserviceProxy;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

@Named("FEC")
public class MoonFecProvider implements MoonInfoProvider {
    private final MoonFrontendProvider moonFrontendProvider;

    @Inject
    protected MoonFecProvider(Map<String, GenericDatapointConfiguration> datapointConfigurations, MoonWebserviceProxy moonWebserviceProxy, ColourAssigner colourAssigner) {
        this.moonFrontendProvider = new MoonFrontendProvider(moonWebserviceProxy,
                colourAssigner,
                this::getDatapointType,
                this::getDataValidityDPE,
                manufacturer -> getConfiguration("FEC", datapointConfigurations)
        );
    }

    private String getDatapointType() {
        return "IcemonFec";
    }

    private String getDataValidityDPE() {
        return "ProcessInput.publicationsValid";
    }

    @Override
    public MoonInfoProvider getConcreteProvider() {
        return moonFrontendProvider;
    }
}
