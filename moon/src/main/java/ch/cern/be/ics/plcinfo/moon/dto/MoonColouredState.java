package ch.cern.be.ics.plcinfo.moon.dto;

public class MoonColouredState {
    private final String state;
    private final StatusColourEnum colour;

    public MoonColouredState(String state, StatusColourEnum colour) {
        this.state = state;
        this.colour = colour;
    }

    public String getState() {
        return state;
    }

    public StatusColourEnum getColour() {
        return colour;
    }
}
