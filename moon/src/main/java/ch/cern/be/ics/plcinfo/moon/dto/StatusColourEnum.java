package ch.cern.be.ics.plcinfo.moon.dto;

public enum StatusColourEnum {
    RED, GREEN, YELLOW, BLUE
}
