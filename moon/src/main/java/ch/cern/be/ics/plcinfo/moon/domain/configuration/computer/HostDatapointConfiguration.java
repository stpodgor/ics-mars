package ch.cern.be.ics.plcinfo.moon.domain.configuration.computer;

import ch.cern.be.ics.plcinfo.moon.domain.configuration.GenericDatapointConfiguration;
import ch.cern.be.ics.plcinfo.moon.dto.MoonDpConfig;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named("COMPUTER_CONFIGURATION")
public class HostDatapointConfiguration implements GenericDatapointConfiguration {
    private final List<MoonDpConfig> hostInfoMetadata = new ArrayList<>();

    public HostDatapointConfiguration() {
        hostInfoMetadata.add(new MoonDpConfig(".readings.power_status", "Power Status"));
        hostInfoMetadata.add(new MoonDpConfig("/Monitoring/Memory.readings.total", "Total Memory"));
        hostInfoMetadata.add(new MoonDpConfig("/Monitoring/Memory.readings.free", "Free Memory"));
        hostInfoMetadata.add(new MoonDpConfig("/Monitoring/Memory.readings.total_swap", "Total Swap"));
        hostInfoMetadata.add(new MoonDpConfig("/Monitoring/Memory.readings.free_swap", "Free Swap"));
        hostInfoMetadata.add(new MoonDpConfig("/Monitoring/Memory.readings.userMemory", "User Memory"));
        hostInfoMetadata.add(new MoonDpConfig("/Monitoring/Memory.readings.usedSwap", "Used Swap"));
        hostInfoMetadata.add(new MoonDpConfig("/Monitoring/Cpus.readings.averageLoad.idle", "CPU Idle (%)"));
    }

    @Override
    public List<MoonDpConfig> getInfoMetadata() {
        return hostInfoMetadata;
    }
}
