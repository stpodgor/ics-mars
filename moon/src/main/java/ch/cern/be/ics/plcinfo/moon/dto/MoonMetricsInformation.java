package ch.cern.be.ics.plcinfo.moon.dto;

import java.util.List;

public class MoonMetricsInformation {
    private final List<MoonColourDp> values;

    public MoonMetricsInformation(List<MoonColourDp> values) {
        this.values = values;
    }

    public List<MoonColourDp> getValues() {
        return values;
    }
}
