package ch.cern.be.ics.plcinfo.moon.dto;

public class MoonData {
    private final String name;
    private final String type;
    private final MoonDeviceInformation moonDeviceInformation;

    public MoonData(String name, String type, MoonDeviceInformation moonDeviceInformation) {
        this.name = name;
        this.type = type;
        this.moonDeviceInformation = moonDeviceInformation;
    }

    public MoonData(String name, String type) {
        this(name, type, new MoonDeviceInformation());
    }

    public MoonData() {
        this("No data found", "", new MoonDeviceInformation());
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public MoonDeviceInformation getMoonDeviceInformation() {
        return moonDeviceInformation;
    }
}
