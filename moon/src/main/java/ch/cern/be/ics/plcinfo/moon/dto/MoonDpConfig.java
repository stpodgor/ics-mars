package ch.cern.be.ics.plcinfo.moon.dto;

public class MoonDpConfig {
    private final String dp;
    private final String description;

    public MoonDpConfig(String dp, String description) {
        this.dp = dp;
        this.description = description;
    }

    public String getDp() {
        return dp;
    }

    public String getDescription() {
        return description;
    }
}
