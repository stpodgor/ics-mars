package ch.cern.be.ics.plcinfo.moon.domain.configuration.plc;

import ch.cern.be.ics.plcinfo.moon.domain.configuration.GenericDatapointConfiguration;
import ch.cern.be.ics.plcinfo.moon.dto.MoonDpConfig;

import java.util.Arrays;
import java.util.List;

public abstract class BasePlcDatapointConfiguration implements GenericDatapointConfiguration {
    @Override
    public List<MoonDpConfig> getInfoMetadata() {
        return Arrays.asList(new MoonDpConfig("ProcessInput.PLCtime", "PLC Time"),
                new MoonDpConfig("ProcessInput.PLCdiagStr", "PLC diagnostic buffer"));
    }
}
