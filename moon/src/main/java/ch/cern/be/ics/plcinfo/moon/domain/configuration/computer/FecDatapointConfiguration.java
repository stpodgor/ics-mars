package ch.cern.be.ics.plcinfo.moon.domain.configuration.computer;

import ch.cern.be.ics.plcinfo.moon.domain.configuration.GenericDatapointConfiguration;
import ch.cern.be.ics.plcinfo.moon.dto.MoonDpConfig;

import javax.inject.Named;
import java.util.Arrays;
import java.util.List;

@Named("FEC_CONFIGURATION")
public class FecDatapointConfiguration implements GenericDatapointConfiguration {

    @Override
    public List<MoonDpConfig> getInfoMetadata() {
        return Arrays.asList(
                new MoonDpConfig("ProcessInput.readings.cpu.value", "CPU"),
                new MoonDpConfig("ProcessInput.readings.freeMemory.value", "Free memory"),
                new MoonDpConfig("ProcessInput.readings.inactMemory.value", "Inactive memory"),
                new MoonDpConfig("ProcessInput.readings.uptime.value", "Uptime"),
                new MoonDpConfig("ProcessInput.readings.usedSwap.value", "Used swap"),
                new MoonDpConfig("ProcessInput.processes.countMissing", "Dead processes count"),
                new MoonDpConfig("ProcessInput.processes.listMissing", "List of dead processes")
        );
    }
}
