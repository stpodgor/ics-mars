$(document).ready(function() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        dataType: "json",
        url: "api/inconsistent/data",

        success: function (data) {

            console.log(data);

             try{

                  var tableBody= document.getElementById("tbodyInconsistent");
                  var tableRows = data.incosistencyChecks.A.payload.length;
                  for (i = 0; i < tableRows; i++) {
                      var tr = document.createElement('TR');
                      for (j = 0; j < 7; j++) {
                          var td = document.createElement('TD');
                          if(j==0){
                              td.appendChild(document.createTextNode(data.incosistencyChecks.A.payload[i][4]));
                              tr.appendChild(td);
                          }
                          if(j==1){
                              td.appendChild(document.createTextNode(data.incosistencyChecks.A.payload[i][5]));
                              tr.appendChild(td);
                          }
                          if(j==2){
                              td.appendChild(document.createTextNode(data.incosistencyChecks.A.payload[i][6]));
                              tr.appendChild(td);
                          }
                          if(j==3){
                              td.appendChild(document.createTextNode(data.incosistencyChecks.A.payload[i][3]));
                              tr.appendChild(td);
                          }
                          if(j==4){
                              td.appendChild(document.createTextNode(data.incosistencyChecks.A.payload[i][7]));
                              tr.appendChild(td);
                          }
                          if(j==5){
                              td.appendChild(document.createTextNode(data.incosistencyChecks.A.payload[i][9]));
                              tr.appendChild(td);
                          }
                          if(j==6){
                              td.appendChild(document.createTextNode(data.incosistencyChecks.A.payload[i][10]));
                              tr.appendChild(td);
                          }

                      }
                      tableBody.appendChild(tr);
                  }
              }catch(err){
                  console.log("Table Error");
              }

              /*TABLE B*/
            try{

                var tableBodyB= document.getElementById("tbodyInconsistent");
                var tableRowsB = data.incosistencyChecks.B.payload.length;
                for (i = 0; i < tableRowsB; i++) {
                    var trB = document.createElement('TR');
                    for (j = 0; j < 7; j++) {
                        var tdB = document.createElement('TD');
                        if(j==0){
                            tdB.appendChild(document.createTextNode(data.incosistencyChecks.B.payload[i][4]));
                            trB.appendChild(tdB);
                        }
                        if(j==1){
                            tdB.appendChild(document.createTextNode(data.incosistencyChecks.B.payload[i][5]));
                            trB.appendChild(tdB);
                        }
                        if(j==2){
                            tdB.appendChild(document.createTextNode(data.incosistencyChecks.B.payload[i][6]));
                            trB.appendChild(tdB);
                        }
                        if(j==3){
                            tdB.appendChild(document.createTextNode(data.incosistencyChecks.B.payload[i][3]));
                            trB.appendChild(tdB);
                        }
                        if(j==4){
                            tdB.appendChild(document.createTextNode(data.incosistencyChecks.B.payload[i][7]));
                            trB.appendChild(tdB);
                        }
                        if(j==5){
                            tdB.appendChild(document.createTextNode(data.incosistencyChecks.B.payload[i][9]));
                            trB.appendChild(tdB);
                        }
                        if(j==6){
                            tdB.appendChild(document.createTextNode(data.incosistencyChecks.B.payload[i][10]));
                            trB.appendChild(tdB);
                        }

                    }
                    tableBodyB.appendChild(trB);
                }
            }catch(err){
                console.log("Table B Error");
            }
             /*TABLE C*/
            try{

                var tableBodyC= document.getElementById("tbodyInconsistent");
                var tableRowsC = data.incosistencyChecks.C.payload.length;
                for (i = 0; i < tableRowsC; i++) {
                    var trC = document.createElement('TR');
                    for (j = 0; j < 7; j++) {
                        var tdC = document.createElement('TD');
                        if(j==0){
                            tdC.appendChild(document.createTextNode(data.incosistencyChecks.C.payload[i][4]));
                            trC.appendChild(tdC);
                        }
                        if(j==1){
                            tdC.appendChild(document.createTextNode(data.incosistencyChecks.C.payload[i][5]));
                            trC.appendChild(tdC);
                        }
                        if(j==2){
                            tdC.appendChild(document.createTextNode(data.incosistencyChecks.C.payload[i][6]));
                            trC.appendChild(tdC);
                        }
                        if(j==3){
                            tdC.appendChild(document.createTextNode(data.incosistencyChecks.C.payload[i][3]));
                            trC.appendChild(tdC);
                        }
                        if(j==4){
                            tdC.appendChild(document.createTextNode(data.incosistencyChecks.C.payload[i][7]));
                            trC.appendChild(tdC);
                        }
                        if(j==5){
                            tdC.appendChild(document.createTextNode(data.incosistencyChecks.C.payload[i][9]));
                            trC.appendChild(tdC);
                        }
                        if(j==6){
                            tdC.appendChild(document.createTextNode(data.incosistencyChecks.C.payload[i][10]));
                            trC.appendChild(tdC);
                        }

                    }
                    tableBodyC.appendChild(trC);
                }
            }catch(err){
                console.log("Table C Error");
            }
             /*TABLE D*/
            try{

                var tableBodyD= document.getElementById("tbodyInconsistent");
                var tableRowsD = data.incosistencyChecks.D.payload.length;
                for (i = 0; i < tableRowsD; i++) {
                    var trD = document.createElement('TR');
                    for (j = 0; j < 7; j++) {
                        var tdD = document.createElement('TD');
                        if(j==0){
                            tdD.appendChild(document.createTextNode(data.incosistencyChecks.D.payload[i][4]));
                            trD.appendChild(tdD);
                        }
                        if(j==1){
                            tdD.appendChild(document.createTextNode(data.incosistencyChecks.D.payload[i][5]));
                            trD.appendChild(tdD);
                        }
                        if(j==2){
                            tdD.appendChild(document.createTextNode(data.incosistencyChecks.D.payload[i][6]));
                            trD.appendChild(tdD);
                        }
                        if(j==3){
                            tdD.appendChild(document.createTextNode(data.incosistencyChecks.D.payload[i][3]));
                            trD.appendChild(tdD);
                        }
                        if(j==4){
                            tdD.appendChild(document.createTextNode(data.incosistencyChecks.D.payload[i][7]));
                            trD.appendChild(tdD);
                        }
                        if(j==5){
                            tdD.appendChild(document.createTextNode(data.incosistencyChecks.D.payload[i][9]));
                            trD.appendChild(tdD);
                        }
                        if(j==6){
                            tdD.appendChild(document.createTextNode(data.incosistencyChecks.D.payload[i][10]));
                            trD.appendChild(tdD);
                        }

                    }
                    tableBodyD.appendChild(trD);
                }
            }catch(err){
                console.log("Table D Error");
            }
             /*TABLE E*/
            try{

                var tableBodyE= document.getElementById("tbodyInconsistent");
                var tableRowsE = data.incosistencyChecks.E.payload.length;
                for (i = 0; i < tableRowsE; i++) {
                    var trE = document.createElement('TR');
                    for (j = 0; j < 7; j++) {
                        var tdE = document.createElement('TD');
                        if(j==0){
                            tdE.appendChild(document.createTextNode(data.incosistencyChecks.E.payload[i][4]));
                            trE.appendChild(tdE);
                        }
                        if(j==1){
                            tdE.appendChild(document.createTextNode(data.incosistencyChecks.E.payload[i][5]));
                            trE.appendChild(tdE);
                        }
                        if(j==2){
                            tdE.appendChild(document.createTextNode(data.incosistencyChecks.E.payload[i][6]));
                            trE.appendChild(tdE);
                        }
                        if(j==3){
                            tdE.appendChild(document.createTextNode(data.incosistencyChecks.E.payload[i][3]));
                            trE.appendChild(tdE);
                        }
                        if(j==4){
                            tdE.appendChild(document.createTextNode(data.incosistencyChecks.E.payload[i][7]));
                            trE.appendChild(tdE);
                        }
                        if(j==5){
                            tdE.appendChild(document.createTextNode(data.incosistencyChecks.E.payload[i][9]));
                            trE.appendChild(tdE);
                        }
                        if(j==6){
                            tdE.appendChild(document.createTextNode(data.incosistencyChecks.E.payload[i][10]));
                            trE.appendChild(tdE);
                        }

                    }
                    tableBodyE.appendChild(trE);
                }
            }catch(err){
                console.log("Table D Error");
            }

            try{
                document.getElementById("contentGenerate").innerHTML = data.incosistencyChecks.A.header[0];
            }catch(err){
                console.log("Date error");
            }
        },
        error: function (data, errorThrown) {
            console.log(errorThrown);
        }
    });

});







