function processData(userId) {
    setTimeout(function () {
        $(".se-pre-con").fadeOut("slow");
    }, 2000);

    function getQueryParam(param) {
        var found = null;
        window.location.search.substr(1).split("&").forEach(function (item) {
            if (param == item.split("=")[0]) {
                found = item.split("=")[1];
            }
        });
        return found;
    }

    var name = getQueryParam('name');

    if (getQueryParam('code') !== null) {
        $.ajax({
            url: "api/identifier/asset?code=" + getQueryParam('code'),
            async: false,
            success: function (data) {
                name = data.potentialIdentifiers;
            }
        });
    } else {
        $.ajax({
            url: "api/identifier/position?code=" + name,
            async: false,
            success: function (data) {
                name = data.potentialIdentifiers;
            }
        });
    }
    name = name.join('&code=');

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        url: "api/infor/position?code=" + name,
        success: callbackInfor
    });
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        url: "api/adams/access?code=" + name + "&userId=" + userId,
        success: callbackAdams
    });
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        url: "api/moon/deviceInfo?code=" + name,
        success: callbackMoon
    });
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        url: "api/landb/general?code=" + name,
        success: callbackLanDb
    });
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        url: "api/icesas/general?code=" + name,
        success: callbackIceSas
    });
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        url: "api/map/location?code=" + name,
        success: callbackMap
    });
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        url: "api/impact/general?" + "userId=" + userId,
        success: callbackImpact
    });
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        cache: false,
        url: "api/infor/workOrders?code=" + name,
        success: callbackWorkOrders
    });

    function callbackWorkOrders(data) {
        console.log("WorkOrders");
        console.log(data);
        try {
            var tableBody = document.getElementById("tbodyWorks");
            var tableRows = data.workOrders.length;

            for (i = 0; i < tableRows; i++) {
                var tr = document.createElement('TR');
                for (j = 0; j < 8; j++) {
                    var td = document.createElement('TD');
                    if (j == 0) {
                        td.appendChild(document.createTextNode(data.workOrders[i].workOrderId));
                        tr.appendChild(td);
                    }
                    if (j == 1) {
                        td.appendChild(document.createTextNode(data.workOrders[i].status));
                        tr.appendChild(td);
                    }
                    if (j == 2) {
                        td.appendChild(document.createTextNode(data.workOrders[i].equipmentName));
                        tr.appendChild(td);
                    }
                    if (j == 3) {
                        td.appendChild(document.createTextNode(data.workOrders[i].description));
                        tr.appendChild(td);
                    }
                    if (j == 4) {
                        td.appendChild(document.createTextNode(data.workOrders[i].department));
                        tr.appendChild(td);
                    }
                    if (j == 5) {
                        td.appendChild(document.createTextNode(data.workOrders[i].orderClass));
                        tr.appendChild(td);
                    }
                    if (j == 6) {
                        td.appendChild(document.createTextNode(data.workOrders[i].reportedBy));
                        tr.appendChild(td);
                    }
                    if (j == 7) {
                        td.appendChild(document.createTextNode(data.workOrders[i].scheduledStart));
                        tr.appendChild(td);
                    }
                }
                tableBody.appendChild(tr);
            }

        } catch (err) {
            console.log("WorkOrders error");
        }
    }

    function callbackImpact(data) {
        console.log("IMPACT");
        console.log(data);
        try {
            var tableBody = document.getElementById("tbody");
            var tableRows = data.data.length;
            for (i = 0; i < tableRows; i++) {
                var tr = document.createElement('TR');
                for (j = 0; j < 5; j++) {
                    var td = document.createElement('TD');
                    if (j == 0) {
                        var link = document.createElement('a');
                        link.setAttribute('href', data.data[i].link);
                        link.setAttribute("target", "_blank");
                        link.appendChild(document.createTextNode(data.data[i].title));
                        //console.log(data.impactData.data[i].link);
                        td.appendChild(link);
                        tr.appendChild(td);
                    }
                    if (j == 1) {
                        td.appendChild(document.createTextNode(data.data[i].facility.label));
                        tr.appendChild(td);
                    }
                    if (j == 2) {
                        td.appendChild(document.createTextNode(data.data[i].description));
                        tr.appendChild(td);
                    }
                    if (j == 3) {
                        td.appendChild(document.createTextNode(data.data[i].status.label));
                        tr.appendChild(td);
                    }
                    if (j == 4) {
                        td.appendChild(document.createTextNode(""));
                        tr.appendChild(td);

                        var lenghtApprovals = data.data[i].approvals.length;
                        var list = document.createElement('ul');

                        for (k = 0; k < lenghtApprovals; k++) {
                            var listInside = document.createElement('li');
                            var approvalTypeLabel = [data.data[i].approvals[k].approvalType.label];
                            var approvalStatusLabel = [data.data[i].approvals[k].approvalStatus.label];
                            listInside.appendChild(document.createTextNode(approvalTypeLabel));

                            listInside.appendChild(document.createElement("br"));
                            listInside.appendChild(document.createTextNode(approvalStatusLabel));
                            list.appendChild(listInside);

                            td.appendChild(list);
                            //  console.log(list);
                        }
                    }
                }
                tableBody.appendChild(tr);
            }
        } catch (err) {
            console.log("Impact error");
        }
    }

    function callbackMap(data) {
        console.log("MAP");
        console.log(data);
        try {
            document.getElementById("jsonLocation").src = data;
        } catch (err) {
            console.log("lanDB error");
        }
    }

    function callbackIceSas(data) {
        console.log("SCADA");
        console.log(data);
        try {
            $('#jsonHost').json2html(data, host);
            $('#jsonProjectName').json2html(data, projectName);
            $('#jsonReduHost').json2html(data, reduHost);

            var objUnicos = data.comments;

            for (var i = 0; i < objUnicos.length; i++) {
                objUnicos = objUnicos.replace('[', '{').replace(']', '}');
            }
            //console.log(objUnicos);
            var objU = JSON.parse(objUnicos);
            //console.log(objU);

            $.each(objU.Application, function (k, v) {
                //console.log(k + ' : ' + v);
                var unicosApp = document.getElementById("unicosApp");
                var content = document.createTextNode(k + ' : ' + v);
                unicosApp.appendChild(document.createElement("br"));
                unicosApp.appendChild(content);
            });

            $.each(objU.Resources, function (k, v) {
                //console.log(k + ' : ' + v);
                var unicosResources = document.getElementById("unicosResources");
                var content = document.createTextNode(k + ' : ' + v);
                unicosResources.appendChild(document.createElement("br"));
                unicosResources.appendChild(content);
            });


        } catch (err) {
            console.log("Scada error");
        }

    }

    function callbackLanDb(data) {
        console.log("LANDB");
        console.log(data);

        $('#jsonDT').json2html(data, transformDT);
        $('#jsonMaker').json2html(data, transformMaker);
        $('#jsonHT').json2html(data, transformHT);
        $('#jsonGroup').json2html(data, transformjsonGroup);
        // $('#jsonDD').json2html(data, transformDD);
        $('#jsonRespUser').json2html(data, transformRespUser);
        $('#jsonTitle').json2html(data, transformTitle);
        $('#json').json2html(data, transformInfo);

    }

    function callbackMoon(data) {
        console.log("MOON");
        console.log(data);
        var length = 0;
        var transformDiagnostics = [];

        try {
            length = data.moonDeviceInformation.metrics.values.length;
            // console.log(length);

            for (i = 0; i < length; i++) {
                transformDiagnostics = [data.moonDeviceInformation.metrics.values[i].moonDpConfig.description];
                var colors = [data.moonDeviceInformation.metrics.values[i].colour];
                // console.log(colors);
                var div = document.createElement('div');
                div.setAttribute('class', colors);
                // temporary parent node, and get the innerHTML content of it:
                div.appendChild(document.createTextNode(""));

                var tmp = document.createElement("div");
                tmp.appendChild(div);
                //console.log(tmp.innerHTML);
                document.getElementById("jsonDiagnostics").innerHTML += tmp.innerHTML + transformDiagnostics;
            }

            length = data.moonDeviceInformation.info.data.length;
            for (i = 0; i < length; i++) {
                var label = data.moonDeviceInformation.info.data[i].moonDpConfig.description;
                var value = data.moonDeviceInformation.info.data[i].value;
                var div = document.createElement('div');
                div.innerHTML += '<b>' + label + '</b><br/>' + value + '<br/>';
                document.getElementById("moonInfoData").appendChild(div)
            }
        } catch (err) {
            console.log("Moon error");
        }

        /*******Color State PLC*****/
        try {
            var stateGralColor = [data.moonDeviceInformation.state.colour];
            var stateGral = [data.moonDeviceInformation.state.state];


            var divColor = document.createElement('div');

            divColor.setAttribute('class', stateGralColor);
            divColor.appendChild(document.createTextNode(stateGral));

            var tmp2 = document.createElement("div");
            tmp2.appendChild(divColor);

            document.getElementById("jsonState").innerHTML = tmp2.innerHTML;

        } catch (err) {
            console.log("Moon error");

            $("#jsonState").html(": Moon not reachable").addClass("reachable");

        }

    }

    function callbackAdams(data) {
        console.log("ADAMS");
        console.log(data);
        try {
            var tableBodyAdams = document.getElementById("tbodyAdams");
            var tableRowsAdams = data.accessPointsInformation.length;

            for (i = 0; i < tableRowsAdams; i++) {
                var trAdams = document.createElement('TR');
                for (j = 0; j < 3; j++) {
                    var tdAdams = document.createElement('TD');
                    if (j == 0) {
                        if (data.accessPointsInformation[i].granted == false) {
                            tdAdams.style.width = 10;
                            tdAdams.style.height = 10;
                            tdAdams.style.backgroundColor = "red";
                            trAdams.appendChild(tdAdams);
                        } else {
                            tdAdams.style.backgroundColor = "green";
                            trAdams.appendChild(tdAdams);
                        }
                    }
                    if (j == 1) {
                        tdAdams.appendChild(document.createTextNode(data.accessPointsInformation[i].accessPointCode));
                        trAdams.appendChild(tdAdams);
                    }
                    if (j == 2) {
                        //  tdAdams.appendChild(document.createTextNode(""));
                        // trAdams.appendChild(tdAdams);

                        var lenghtAccessPointData = data.accessPointsInformation[i].accessPointData.length;
                        var listAdams = document.createElement('ul');

                        for (k = 0; k < lenghtAccessPointData; k++) {
                            var listInsideAdams = document.createElement('li');
                            var labelDescription = [data.accessPointsInformation[i].accessPointData[k].description];
                            listInsideAdams.appendChild(document.createTextNode(labelDescription));
                            var labelUrl;
                            if (data.accessPointsInformation[i].accessPointData[k].url != null) {
                                // tdAdams.appendChild(listAdams);

                                labelUrl = document.createElement('a');
                                labelUrl.setAttribute('href', data.accessPointsInformation[i].accessPointData[k].url);
                                labelUrl.setAttribute("target", "_blank");
                                labelUrl.appendChild(document.createTextNode(data.accessPointsInformation[i].accessPointData[k].urlTitle));
                                listInsideAdams.appendChild(labelUrl);
                            }
                            listAdams.appendChild(listInsideAdams);
                            tdAdams.appendChild(listAdams);
                            trAdams.appendChild(tdAdams);
                        }
                    }
                }
                tableBodyAdams.appendChild(trAdams);
            }
        } catch (err) {
            console.log("Adams error");
        }
    }

    function callbackInfor(data) {
        console.log("INFOR");
        console.log(data);
        try {
            var divTitleComponents = document.getElementById("plcComposition");
            for (i = 0; i < data.children.components.length; i++) {
                divTitleComponents.appendChild(document.createTextNode(data.children.components[i].description));
                var listPlcComposition = document.createElement('ul');

                for (j = 0; j < data.children.components[i].children.length; j++) {
                    var listInsidePlcComposition = document.createElement('li');
                    var labelInsideList = data.children.components[i].children[j].description;
                    var nameInsideList = data.children.components[i].children[j].name;
                    listInsidePlcComposition.appendChild(document.createTextNode(labelInsideList));
                    listInsidePlcComposition.appendChild(document.createTextNode(" "));
                    listInsidePlcComposition.appendChild(document.createTextNode(nameInsideList));
                    listPlcComposition.appendChild(listInsidePlcComposition);
                    divTitleComponents.appendChild(listPlcComposition);
                }
            }

        } catch (err) {
            console.log("InforEamInfo error");
        }
    }


    var transformDT = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Device Type: "},
            {"tag": "span", "html": "${genericType}"}
        ]
        }];
    var transformMaker = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Maker: "},
            {"tag": "span", "html": "${maker}"}
        ]
        }];
    var transformHT = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Hardware Type: "},
            {"tag": "span", "html": "${hardwareType}"}
        ]
        }];
    var transformTitle = [
        {"tag": "p", "html": "${name}"}];

    var transformInfo = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Description: "},
            {"tag": "span", "html": "${description}"}
        ]
        },
        /* {"tag":"div","children":[
         {"tag":"b","html":"Asset Description: "},
         {"tag":"span","html":"${assetDescription}"},
         ]},*/
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Location: "},
            {"tag": "span", "html": "${location}"},
        ]
        },
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Outlet: "},
            {"tag": "span", "html": "${outlet}"}
        ]
        }];

    var transformjsonGroup = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Group: "},
            {"tag": "span", "html": "${group}"}
        ]
        }];
    /* var transformDD = [
     {"tag":"div","children":[
     {"tag":"b","html":"Department Description: "},
     {"tag":"span","html":"${departmentDesc}"}
     ]}];*/
    var transformRespUser = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Responsible Person: "},
            {"tag": "span", "html": "${responsiblePerson}"},
        ]
        },
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Main User: "},
            {"tag": "span", "html": "${mainUser}"}]

        }];
    var moonDeviceInfo = [
        {"tag": "p", "html": "${moonDeviceInformation.info.data.0.moonDpConfig.description}"}];
    var moonDeviceInfoTime = [
        {"tag": "p", "html": "${moonDeviceInformation.info.data.0.value}"}];
    var host = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Host: "},
            {"tag": "span", "html": "${hostName}"}
        ]
        }];
    var projectName = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "WinCC OA Project: "},
            {"tag": "span", "html": "${projectName}"}
        ]
        }];
    var reduHost = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Redundant Host: "},
            {"tag": "span", "html": "${redundantHost}"}
        ]
        }];
    var unicosApplic = [
        {
            "tag": "div", "children": [
            {"tag": "b", "html": "Unicos Application: "},
            {"tag": "span", "html": "${unicosApplication}"}
        ]
        }];
}