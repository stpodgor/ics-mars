<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<@spring.bind "data" />

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mars</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/reset.css" rel="stylesheet" type="text/css">
    <link href="css/toolbar.css" rel="stylesheet" type="text/css">
    <link href="css/mdb.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/cern-bootstrap-mdl-customizations.css" rel="stylesheet">
    <link href="css/robo.css" rel="stylesheet" type='text/css'>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.json2html.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/modernizr.js"></script>

</head>
<style>

    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(simple-pre-loader/images/loader-128x/preload_400.gif) center no-repeat #fff;
    }
    .loadingInfo{
        margin-top: 5%;
        margin-left: 10%;
        text-align: center;
        font-size: 30px;
    }

</style>
<body class="has-drawer" onload="processData('${data.userId}')">
<div class="se-pre-con">
    <div class="loadingInfo">
        MARS is fetching all known information about the device from all different sources. Please, wait...
    </div>
</div>
<div id="cern-toolbar">
    <h1><a href="http://home.cern" title="CERN">CERN <span>Accelerating science</span></a></h1>
    <ul class="cern-signedin">
        <li class="cern-accountlinks"><span>Signed in as: <a class="account" href="http://cern.ch/account"
                                                             title="Signed in as ${data.userName} (${data.userLogin})">${data.userLogin}
            (CERN)</a> </span><a
                class="cern-signout" href="#" title="Sign out of your account">Sign out</a></li>
        <li><a class="cern-directory" href="http://cern.ch/directory"
               title="Search CERN resources and browse the directory">Directory</a></li>
    </ul>
</div>

<div id="wrapper" class="headerContainerWrapper">
    <nav class="navbar navbar-default navbar-static-top headerContainerShadow" role="navigation"
         style="margin-bottom: 0">
        <div class="navbar-header headerContainer">

            <div class="navbar-brand" style="white-space: nowrap;" id="jsonTitle"></div>

        </div>
    </nav>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <div id="jsonText"> Device State
                    <div id="jsonState"></div><!--general state plc:color-->
                </div>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel-group" id="accordion">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="" data-toggle="collapse" href="#collapseOne">
                                <i class="fa fa-bar-chart-o fa-fw">INFO</i>
                            </a>
                        </h4>
                    </div><!--/.panel-heading -->
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div id="deviceInfo" class="columns">
                                <ul>
                                    <li id="jsonDT"></li><!--Device Type-->
                                    <li id="jsonMaker"></li><!--Maker-->
                                    <li id="jsonHT"></li><!--Hardware Type-->
                                </ul>
                            </div>
                            <div id="json"></div><!--Description,Asset Description,Location-->
                            <table id="personInfo">
                                <tr>
                                    <td id="jsonGroup"></td><!--Group-->
                                    <td id="jsonDD"></td><!--Department Description-->
                                </tr>
                            </table>
                            <div id="jsonRespUser"></div><!--Responsible Person, Main User-->

                        </div><!--/.panel-body -->
                    </div><!--/.panel-collapse -->
                </div><!-- /.panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="" data-toggle="collapse" href="#collapseTwo">
                                <i class="fa fa-bar-chart-o fa-fw">DIAGNOSTICS</i>
                            </a>
                        </h4>
                    </div><!--/.panel-heading -->
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <ul id="diagnostics">
                                <li id="jsonDiagnosticsTitleTime"></li>
                            </ul>
                            <div id="jsonDiagnostics" style="position: relative; float: left; width:45%"></div>
                            <!--label: colour flags-->
                            <div id="moonInfoData"
                                 style="position: relative;float: left;  margin-left:5%; width:45%"></div>
                        </div><!--label: colour flags-->
                    </div><!--/.panel-body -->
                </div><!--/.panel-collapse -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="" data-toggle="collapse" href="#collapseFour">
                                <i class="fa fa-bar-chart-o fa-fw">UNICOS Info</i>
                            </a>
                        </h4>
                    </div><!--/.panel-heading -->
                    <div id="collapseFour" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div id="jsonHost"></div>
                            <div id="jsonProjectName"></div>
                            <div id="jsonUnicosApplic"></div>
                            <div id="jsonReduHost"></div>
                            <div class="panel panel-default half-panel-left">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw" >Application</i>
                                </div>
                                <div class="panel-body"  id="unicosApp">

                                </div>
                            </div>
                            <div class="panel panel-default half-panel-right">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw">Resources</i>
                                </div>
                                <div class="panel-body" id="unicosResources">
                                </div>
                            </div>
                        </div><!--/.panel-body -->
                    </div><!--/.panel-collapse -->
                </div><!-- /.panel -->


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="" data-toggle="collapse" href="#collapseFive">
                                <i class="fa fa-bar-chart-o fa-fw">PLC</i>
                            </a>
                        </h4>
                    </div><!--/.panel-heading -->
                    <div id="collapseFive" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div id="plcComposition"></div>
                        </div><!--/.panel-body -->
                    </div><!--/.panel-collapse -->
                </div><!-- /.panel -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="" data-toggle="collapse" href="#collapseSix">
                                <i class="fa fa-bar-chart-o fa-fw">Work Order</i>
                            </a>
                        </h4>
                    </div><!--/.panel-heading -->
                    <div id="collapseSix" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table id="worksTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Status</th>
                                        <th>Description</th>
                                        <th>Department</th>
                                        <th>Equipment Name</th>
                                        <th>Order Class</th>
                                        <th>Reported By</th>
                                        <th>scheduled Start</th>
                                    </tr>
                                <thead>
                                <tbody id="tbodyWorks">
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div><!-- /.panel -->


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapseThree">
                                <i class="fa fa-bar-chart-o fa-fw">LOCATION</i>
                            </a>
                        </h4>
                    </div><!--/.panel-heading -->
                    <div id="collapseThree" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div id="mydiv">
                                <iframe id="jsonLocation" scrolling="no"></iframe>
                            </div>

                            <div class="panel panel-default panelsInsideAccess">

                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw">ACCESS POINT</i>
                                </div>
                                <div class="panel-body">
                                    <div id="accessConditions">
                                        <a href="http://service-access-data.web.cern.ch/service-access-data/lhc_access_conditions.asp"> Check Access Conditions</a>
                                    </div>
                                   <!-- <div id="userLed"> <p>ACCESS GRANTED TO ${data.userName}</p>
                                        <div style="width: 15px;height: 15px;border:1px solid #000;margin-top: -29px;margin-left: -18px;float: left;background-color: green;"> </div></div>-->
                                    <table id="adamsTable">
                                        <thead>
                                        <tr>
                                            <th>Access</th>
                                            <th>Access Point</th>
                                            <th>Comments</th>

                                        <tr>
                                        <thead>
                                        <tbody id="tbodyAdams">
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="panel panel-default panelsInsideAccess">

                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw">IMPACT</i>
                                </div>
                                <div class="panel-body">
                                    <div class="panel-heading" style="overflow-x:auto;">
                                        <table id="impactTable">
                                            <thead>
                                            <tr>
                                                <th>Impact Request</th>
                                                <th>Facility</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Approvals</th>
                                            <tr>
                                            <thead>
                                            <tbody id="tbody">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div><!--/.panel-body -->
                    </div><!--/.panel-collapse -->
                </div><!-- /.panel -->



            </div><!-- /.panel-group -->

        </div><!--col-lg-8-->
    </div>
</div>

<script src="js/json2html.js"></script>
</body>
</html>