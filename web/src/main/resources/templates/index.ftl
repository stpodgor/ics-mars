<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <link href="css/home.css" rel="stylesheet" type="text/css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/HomeScript.js"></script>
</head>
<body>
<form id="contactName" action="/name" method="GET" onsubmit="document.location.href = 'query?name='+this.name.value; return false;">
    <div class="container">
        <div class="head">
            <h2>Name</h2>
        </div>
        <input id="name" type="text" name="name" placeholder="Equipment name:" /><br />
        <div class="message">Querying</div>
        <button id="submit" type="submit">
            Send
        </button>
    </div>
</form>
<form id="equipmentID" action="/name" method="GET"
      onsubmit="document.location.href = 'query?code='+this.id.value; return false;">
    <div class="container">
        <div class="head">
            <h2>ID</h2>
        </div>
        <input id="id" type="text" name="name" placeholder="Equipment id:" /><br />
        <div class="message">Querying</div>
        <button id="submit" type="submit">
            Send
        </button>
    </div>
</form>
</body>
</html>