<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<@spring.bind "data" />

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Table</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="css/table.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-table.min.css" rel="stylesheet" type="text/css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.json2html.js"></script>
    <script src="js/table.js"></script>
    <script src="js/bootstrapTable.js"></script>
    <script src="js/bootstrap-table-locale-all.min.js"></script>

</head>

<body>

    <div>
        <h2>Contents Generated at:<p id="contentGenerate"/></h2>

        <div class="row-fluid">
            <table class="table table-hover table-striped table-condensed table-bordered" id="table" data-search="true" data-show-columns="true" data-pagination="true">
                <thead>
                    <tr>
                        <th data-field="4" data-sortable="true">Device Name</th>
                        <th data-field="5" data-sortable="true">Device Type</th>
                        <th data-field="6" data-sortable="true">Responsible</th>
                        <th data-field="3" data-sortable="true">Problem Description</th>
                        <th data-field="7" data-sortable="true">Domain</th>
                        <th data-field="9" data-sortable="true">Model</th>
                        <th data-field="10" data-sortable="true">Additional Info</th>
                    </tr>
                </thead>
                <tbody id="tbodyInconsistent">
                </tbody>
            </table>
        </div>
    </div>



</body>

</html>