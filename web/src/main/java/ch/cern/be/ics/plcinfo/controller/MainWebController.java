package ch.cern.be.ics.plcinfo.controller;

import ch.cern.be.ics.plcinfo.commons.exception.DataCollectionException;
import ch.cern.be.ics.plcinfo.userinfo.dto.SSOBean;
import ch.cern.be.ics.plcinfo.userinfo.filter.SSOFilter;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;

@Controller
@RequestMapping("/")
public class MainWebController implements ErrorController {

    private final SSOFilter ssoFilter;

    @Inject
    public MainWebController(SSOFilter ssoFilter) {
        this.ssoFilter = ssoFilter;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView renderSelectionForm() throws DataCollectionException {
        return new ModelAndView("index");
    }

    @RequestMapping(value = "query", method = RequestMethod.GET)
    public ModelAndView getDataByName() throws DataCollectionException {
        ModelAndView main = new ModelAndView("main");
        main.addObject("data", new SSOBean(ssoFilter.getUserId(), ssoFilter.getUserLogin(), ssoFilter.getUserName()));
        return main;
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView renderError() throws DataCollectionException {
        return new ModelAndView("error");
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }


    @RequestMapping(value = "/table", method = RequestMethod.GET)
    public ModelAndView view() throws DataCollectionException {
        ModelAndView main = new ModelAndView("table");
        main.addObject("data", new SSOBean(ssoFilter.getUserId(), ssoFilter.getUserLogin(), ssoFilter.getUserName()));
        return main;
    }
}
