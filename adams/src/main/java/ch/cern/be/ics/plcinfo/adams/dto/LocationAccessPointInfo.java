package ch.cern.be.ics.plcinfo.adams.dto;

import java.util.List;

public class LocationAccessPointInfo {
    private final String locationName;
    private final List<AccessPointInfo> accessPointsInformation;

    public LocationAccessPointInfo(String locationName, List<AccessPointInfo> accessPointInfos) {
        this.locationName = locationName;
        this.accessPointsInformation = accessPointInfos;
    }

    public String getLocationName() {
        return locationName;
    }

    public List<AccessPointInfo> getAccessPointsInformation() {
        return accessPointsInformation;
    }
}
