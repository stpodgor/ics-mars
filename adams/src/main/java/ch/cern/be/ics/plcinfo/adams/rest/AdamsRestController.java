package ch.cern.be.ics.plcinfo.adams.rest;

import ch.cern.be.ics.plcinfo.adams.dto.AdamsInfo;
import ch.cern.be.ics.plcinfo.adams.service.AdamsJDBCService;
import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.exception.DataCollectionException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;

@RestController
@RequestMapping("/api/adams")
public class AdamsRestController {

    private final AdamsJDBCService adamsJDBCService;

    @Inject
    public AdamsRestController(AdamsJDBCService adamsJDBCService) {
        this.adamsJDBCService = adamsJDBCService;
    }

    @CrossOrigin
    @RequestMapping(value = "/access", method = RequestMethod.GET)
    public ResponseEntity<AdamsInfo> getAccessPointsInfo(@RequestParam String[] code, @RequestParam String userId) throws DataCollectionException {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(c -> adamsJDBCService.getAccessPointInfo(userId, c), Arrays.stream(code)));
    }
}
