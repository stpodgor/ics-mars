package ch.cern.be.ics.plcinfo.adams.dto;

import java.util.Collections;
import java.util.List;

public class AdamsInfo {
    private final List<AccessPointInfo> accessPointsInformation;

    public AdamsInfo(List<AccessPointInfo> accessPointsInformation) {
        this.accessPointsInformation = accessPointsInformation;
    }

    public AdamsInfo() {
        this(Collections.emptyList());
    }

    public List<AccessPointInfo> getAccessPointsInformation() {
        return accessPointsInformation;
    }
}
