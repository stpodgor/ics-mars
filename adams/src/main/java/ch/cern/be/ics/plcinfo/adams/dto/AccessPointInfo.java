package ch.cern.be.ics.plcinfo.adams.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AccessPointInfo {
    private final String accessPointCode;
    private final List<AccessPointData> accessPointData = new ArrayList<>();

    public AccessPointInfo(String accessPointCode) {
        this.accessPointCode = accessPointCode;
    }

    public String getAccessPointCode() {
        return accessPointCode;
    }

    public List<AccessPointData> getAccessPointData() {
        return accessPointData;
    }

    public void add(AccessPointData accessPointData) {
        this.accessPointData.add(accessPointData);
    }

    public boolean isGranted() {
        return !accessPointData.stream()
                .map(AccessPointData::getDescription)
                .collect(Collectors.joining())
                .contains("not granted");
    }
}
