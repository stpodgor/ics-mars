package ch.cern.be.ics.plcinfo.adams.dto;

public class AccessPointData {
    private final String description;
    private final String urlTitle;
    private final String url;

    public AccessPointData(String description, String urlTitle, String url) {
        this.description = description;
        this.urlTitle = urlTitle;
        if (url != null && url.startsWith("f?")) {
            this.url = "https://apex-sso.cern.ch/pls/htmldb_edmsdb/" + url;
        } else {
            this.url = url;
        }

    }

    public String getDescription() {
        return description;
    }

    public String getUrlTitle() {
        return urlTitle;
    }

    public String getUrl() {
        return url;
    }
}
