package ch.cern.be.ics.plcinfo.adams.service;

import ch.cern.be.ics.plcinfo.adams.dto.AccessPointData;
import ch.cern.be.ics.plcinfo.adams.dto.AccessPointInfo;
import ch.cern.be.ics.plcinfo.adams.dto.AdamsInfo;
import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.service.BaseJdbcService;
import ch.cern.be.ics.plcinfo.commons.service.DbType;
import ch.cern.be.ics.plcinfo.commons.service.JdbcQueryProxy;
import ch.cern.be.ics.plcinfo.impact.dto.LocationAccess;
import ch.cern.be.ics.plcinfo.impact.input.AccessPointDTO;
import ch.cern.be.ics.plcinfo.impact.service.ImpactService;
import ch.cern.be.ics.plcinfo.landb.dto.LanDbInfo;
import ch.cern.be.ics.plcinfo.landb.service.LanDbJDBCService;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

@Named
public class AdamsJDBCService extends BaseJdbcService {

    @Inject
    AdamsJDBCService(ImpactService impactService, LanDbJDBCService lanDbJDBCService, PLCInfoLogger logger, JdbcQueryProxy proxy) {
        super(DbType.ADAMS, logger, proxy);
        this.impactService = impactService;
        this.lanDbJDBCService = lanDbJDBCService;
    }

    private final ImpactService impactService;
    private final LanDbJDBCService lanDbJDBCService;

    @Override
    protected String getQuery() {
        return "SELECT * FROM TABLE(ADAMS3.DEV_HOW_TO_ACCESS (?, ?))";
    }

    public Optional<AdamsInfo> getAccessPointInfo(String personId, String functionalPosition) {
        return getAccessPointInfo(personId, functionalPosition, this::getLocation, this::getAccessPoints, this::getAccessPointInfo);
    }

    private Optional<AdamsInfo> getAccessPointInfo(String personId, String functionalPosition, Function<String, Optional<String>> locationSupplier,
                                                   BiFunction<String, String, List<LocationAccess>> accessPointsSupplier,
                                                   BiFunction<String, Map<String, List<String>>, Optional<AdamsInfo>> adamsInfoSupplier) {
        Map<String, List<String>> accessPointCodes = locationSupplier.apply(functionalPosition)
                .map(location -> accessPointsSupplier.apply(location, personId))
                .orElse(Collections.emptyList())
                .stream()
                .collect(Collectors.toMap(
                        locationAccess -> locationAccess.getLocation().getName(),
                        this::getLocationAccessForUser,
                        (x, y) -> x
                ));
        return Utils.collectAsynchronousData(() -> adamsInfoSupplier.apply(personId, accessPointCodes), "adams", logger);

    }

    private Optional<String> getLocation(String functionalPosition) {
        return Utils.collectAsynchronousData(() -> lanDbJDBCService.getDeviceInfo(functionalPosition), "adams", logger)
                .map(LanDbInfo::getLocation);
    }

    private List<LocationAccess> getAccessPoints(String location, String personId) {
        return Utils.collectAsynchronousData(() -> impactService.getLocationAccess(personId, location), "impact", logger)
                .orElse(Collections.emptyList());
    }

    private List<String> getLocationAccessForUser(LocationAccess locationAccess) {
        List<AccessPointDTO> accessPoints = new ArrayList<>();
        accessPoints.addAll(locationAccess.getAccessPoints().getGeneralAccessPoints());
        accessPoints.addAll(locationAccess.getAccessPoints().getRestrictedAccessPoints());
        return accessPoints.stream().map(AccessPointDTO::getName).collect(Collectors.toList());
    }

    public Optional<AdamsInfo> getAccessPointInfo(String personId, Map<String, List<String>> accessPoints) {
        List<String> distinctAccessPoints = accessPoints.values()
                .stream()
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
        return getAccessPointInfoForDistinctPoints(personId, distinctAccessPoints);
    }

    private Optional<AdamsInfo> getAccessPointInfoForDistinctPoints(String personId, List<String> distinctAccessPoints) {
        List<AccessPointInfo> accessPoints = distinctAccessPoints
                .stream()
                .map(accessPointCode -> getSinglePointInfo(personId, accessPointCode))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
        if (accessPoints.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new AdamsInfo(accessPoints));
    }

    private Optional<AccessPointInfo> getSinglePointInfo(String personId, String accessPointCode) {
        try {
            PreparedStatement preparedStatement = getPreparedQuery();
            preparedStatement.setLong(1, Long.valueOf(personId));
            preparedStatement.setString(2, accessPointCode);
            ResultSet result = preparedStatement.executeQuery();
            return Optional.of(extractAccessPoints(result, accessPointCode));
        } catch (SQLException e) {
            logger.warning("Error when collected data from ADAMS " + e.getMessage());
            logger.warning(e);
            restartConnection();
        }
        return Optional.empty();
    }

    private AccessPointInfo extractAccessPoints(ResultSet result, String accessPointCode) throws SQLException {
        AccessPointInfo accessPointInfo = new AccessPointInfo(accessPointCode);
        while (result.next()) {
            String description = result.getString(3);
            String urlTitle = result.getString(4);
            String url = result.getString(5);
            accessPointInfo.add(new AccessPointData(description, urlTitle, url));
        }
        return accessPointInfo;
    }
}
