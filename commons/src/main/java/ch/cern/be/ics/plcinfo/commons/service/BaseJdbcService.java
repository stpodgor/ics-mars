package ch.cern.be.ics.plcinfo.commons.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class BaseJdbcService {

    protected final PLCInfoLogger logger;
    private final JdbcQueryProxy queryProxy;
    private final DbType dbType;

    protected abstract String getQuery();

    protected BaseJdbcService(DbType dbType, PLCInfoLogger logger, JdbcQueryProxy queryProxy) {
        this.dbType = dbType;
        this.logger = logger;
        this.queryProxy = queryProxy;
    }

    protected PreparedStatement getPreparedQuery() throws SQLException {
        return getPreparedQuery(getQuery());
    }

    protected PreparedStatement getPreparedQuery(String query) throws SQLException {
        try {
            return queryProxy.getPreparedQuery(dbType, query);
        } catch (ClassNotFoundException e) {
            throw new SQLException(e.getMessage(), e);
        }
    }

    protected void restartConnection() {
        queryProxy.restartConnection(dbType);
    }
}
