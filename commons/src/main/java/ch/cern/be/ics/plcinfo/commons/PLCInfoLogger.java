package ch.cern.be.ics.plcinfo.commons;

import javax.inject.Named;
import java.util.logging.Level;
import java.util.logging.Logger;

@Named
public class PLCInfoLogger {
    private final Logger logger = Logger.getLogger("PLCInfo");

    public void severe(String msg) {
        logger.severe(msg);
    }

    public void severe(Throwable t) {
        logger.log(Level.SEVERE, t.getMessage(), t);
    }

    public void warning(String msg) {
        logger.warning(msg);
    }

    public void info(String msg) {
        logger.info(msg);
    }

    public void fine(String msg) {
        logger.fine(msg);
    }

    public void warning(Exception e) {
        logger.log(Level.WARNING, e.getMessage(), e);
    }
}
