package ch.cern.be.ics.plcinfo.commons.exception;

public class DataCollectionException extends Exception {
    public DataCollectionException(Exception e) {
        super(e);
    }
}
