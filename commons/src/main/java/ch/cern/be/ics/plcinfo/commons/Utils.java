package ch.cern.be.ics.plcinfo.commons;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Utilities
 */
public class Utils {

    public static <T, S> Optional<T> tryDataUntilNonEmptyResult(Function<S, Optional<T>> function, Stream<S> dataToTry) {
        return dataToTry
                .map(function)
                .filter(Optional::isPresent)
                .findFirst()
                .orElse(Optional.empty());
    }

    public static <T> ResponseEntity<T> formResponse(Optional<T> data) {
        return data.map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null));
    }

    public static <T> Optional<T> collectAsynchronousData(Supplier<Optional<T>> dataSupplier, String errorLabel, PLCInfoLogger logger) {
        BiConsumer<Exception, String> exceptionCallback = (e, s) -> {
            logger.warning("Error during asynchronous data collection for " + errorLabel + ", " + e.toString() + ", " + e.getMessage());
            logger.warning(e);
        };
        return collectAsynchronousData(dataSupplier, errorLabel, exceptionCallback, 10);
    }

    public static <T> Optional<T> collectAsynchronousData(Supplier<Optional<T>> dataSupplier, String errorLabel, BiConsumer<Exception, String> errorCallback, int timeout) {
        return getFromFuture(CompletableFuture.supplyAsync(dataSupplier), errorLabel, errorCallback, timeout);
    }


    public static <T> T getFromFutureOrDefault(Future<Optional<T>> future, T defaultValue, String errorLabel, BiConsumer<Exception, String> errorCallback, int timeout) {
        return getFromFuture(future, errorLabel, errorCallback, timeout)
                .orElse(defaultValue);
    }


    public static <T> Optional<T> getFromFuture(Future<Optional<T>> future, String errorLabel, BiConsumer<Exception, String> errorCallback, int timeout) {
        try {
            return future.get(timeout, TimeUnit.SECONDS);
        } catch (Exception e) {
            errorCallback.accept(e, errorLabel);
            return Optional.empty();
        }
    }
}
