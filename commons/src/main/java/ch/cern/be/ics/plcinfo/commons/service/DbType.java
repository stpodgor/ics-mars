package ch.cern.be.ics.plcinfo.commons.service;

import ch.cern.be.ics.plcinfo.commons.Credentials;

public enum DbType {
    CERN_DB1("oracle.jdbc.OracleDriver", Credentials.login, Credentials.pass, "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=pdb-s.cern.ch) (PORT=10121) )(LOAD_BALANCE=off)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=PDB_CERNDB1.cern.ch)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC))))"),
    ICESAS("oracle.jdbc.OracleDriver", Credentials.icesasLogin, Credentials.icesasPassword, "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=pdb-s.cern.ch) (PORT=10121) )(LOAD_BALANCE=off)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=PDB_CERNDB1.cern.ch)(FAILOVER_MODE=(TYPE=SELECT)(METHOD=BASIC))))"),
    ADAMS("oracle.jdbc.OracleDriver", Credentials.login, Credentials.pass, "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=devdb11-s.cern.ch) (PORT=10121) )(ENABLE=BROKEN)(CONNECT_DATA=(SERVICE_NAME=devdb11_s.cern.ch)))");

    private final String driverName;
    private final String login;
    private final String password;
    private final String url;

    DbType(String driverName, String login, String password, String url) {
        this.driverName = driverName;
        this.login = login;
        this.password = password;
        this.url = url;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getUrl() {
        return url;
    }
}
