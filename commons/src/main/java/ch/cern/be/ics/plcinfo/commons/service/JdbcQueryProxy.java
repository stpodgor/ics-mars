package ch.cern.be.ics.plcinfo.commons.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Named
public class JdbcQueryProxy {

    private final PLCInfoLogger logger;
    private final Map<DbType, Connection> connections = new ConcurrentHashMap<>();

    @Inject
    public JdbcQueryProxy(PLCInfoLogger logger) {
        this.logger = logger;
    }

    PreparedStatement getPreparedQuery(DbType dbType, String query) throws SQLException, ClassNotFoundException {
        Connection connection = connections.get(dbType);
        if (connection != null && !connection.isClosed()) {
            return connection.prepareStatement(query);
        } else {
            connection = initializeConnection(dbType);
            connections.put(dbType, connection);
            return connection.prepareStatement(query);
        }
    }

    private Connection initializeConnection(DbType dbType) throws SQLException, ClassNotFoundException {
        Class.forName(dbType.getDriverName());
        String dbUrl = dbType.getUrl();
        String login = dbType.getLogin();
        String password = dbType.getPassword();
        return DriverManager.getConnection(dbUrl, login, password);
    }

    void restartConnection(DbType dbType) {
        try {
            Connection connection = connections.get(dbType);
            connection.close();
        } catch (SQLException e) {
            logger.warning(e.getMessage());
            // we already know something was wrong...
        }
        try {
            Connection connection = initializeConnection(dbType);
            connections.put(dbType, connection);
        } catch (ClassNotFoundException | SQLException e) {
            // not good
        }
    }
}
