package ch.cern.be.ics.plcinfo.inconsistent.dto;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for storing single inconsistency check data
 */
public class InconsistencyCheck {
    private final List<String> header;
    private final List<List<String>> payload;

    public InconsistencyCheck(List<List<String>> inputData) {
        header = inputData.get(0).
                stream()
                .filter(entry -> !entry.equals("-"))
                .collect(Collectors.toList());
        payload = inputData.subList(1, inputData.size())
                .stream()
                .map(attributeValues -> attributeValues.subList(0, header.size()))
                .collect(Collectors.toList());
    }

    public List<String> getHeader() {
        return header;
    }

    public List<List<String>> getPayload() {
        return payload;
    }
}
