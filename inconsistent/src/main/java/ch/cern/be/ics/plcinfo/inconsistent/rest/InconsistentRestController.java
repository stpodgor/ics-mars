package ch.cern.be.ics.plcinfo.inconsistent.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.exception.DataCollectionException;
import ch.cern.be.ics.plcinfo.inconsistent.dto.InconsistentData;
import ch.cern.be.ics.plcinfo.inconsistent.service.InconsistentJDBCService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/inconsistent")
public class InconsistentRestController {

    private final InconsistentJDBCService inconsistentJdbcService;

    @Inject
    public InconsistentRestController(InconsistentJDBCService inconsistentJdbcService) {
        this.inconsistentJdbcService = inconsistentJdbcService;
    }

    @CrossOrigin
    @RequestMapping(value = "/data", method = RequestMethod.GET)
    public ResponseEntity<InconsistentData> getInconsistentData() throws DataCollectionException {
        return Utils.formResponse(inconsistentJdbcService.getData());
    }
}
