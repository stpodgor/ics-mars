package ch.cern.be.ics.plcinfo.impact.dto;

import ch.cern.be.ics.plcinfo.impact.input.LocationDTO;

public class LocationAccess {
    private final LocationDTO location;
    private final AccessPointsForLocation accessPoints;

    public LocationAccess(LocationDTO location, AccessPointsForLocation accessPoints) {
        this.location = location;
        this.accessPoints = accessPoints;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public AccessPointsForLocation getAccessPoints() {
        return accessPoints;
    }
}
