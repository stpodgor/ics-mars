package ch.cern.be.ics.plcinfo.impact.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.exception.DataCollectionException;
import ch.cern.be.ics.plcinfo.impact.dto.ImpactData;
import ch.cern.be.ics.plcinfo.impact.dto.LocationAccess;
import ch.cern.be.ics.plcinfo.impact.service.ImpactService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/impact")
public class ImpactRestController {
    private final ImpactService impactService;

    @Inject
    public ImpactRestController(ImpactService impactService) {
        this.impactService = impactService;
    }

    @CrossOrigin
    @RequestMapping(value = "/general", method = RequestMethod.GET)
    public ResponseEntity<ImpactData> getImpactData(@RequestParam String userId) throws DataCollectionException {
        return Utils.formResponse(impactService.getData(userId));
    }

    /**
     * Get impact access location data for given functional position. UserId is used as the query author.
     */
    @CrossOrigin
    @RequestMapping(value = "/locationAccess", method = RequestMethod.GET)
    public ResponseEntity<List<LocationAccess>> getImpactLocationData(@RequestParam String userId, @RequestParam String[] code) throws DataCollectionException {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(c -> impactService.getLocationAccessForEquipment(userId, c), Arrays.stream(code)));
    }
}
