package ch.cern.be.ics.plcinfo.impact.service;

import ch.cern.be.ics.plcinfo.impact.dto.ImpactData;
import ch.cern.be.ics.plcinfo.impact.dto.LocationAccess;
import ch.cern.be.ics.plcinfo.impact.input.ActivityDTO;
import ch.cern.be.ics.plcinfo.impact.input.OrgUnitDTO;
import ch.cern.be.ics.plcinfo.impact.input.PagedList;
import ch.cern.be.ics.plcinfo.landb.dto.LanDbInfo;
import ch.cern.be.ics.plcinfo.landb.service.LanDbJDBCService;
import org.springframework.core.ParameterizedTypeReference;

import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Named
public class ImpactService {

    private final ImpactQueryProxy impactQueryProxy;
    private final ImpactLocationService impactLocationService;
    private final LanDbJDBCService lanDbJDBCService;

    @Inject
    public ImpactService(ImpactQueryProxy impactQueryProxy, ImpactLocationService impactLocationService, LanDbJDBCService lanDbJDBCService) {
        this.impactQueryProxy = impactQueryProxy;
        this.impactLocationService = impactLocationService;
        this.lanDbJDBCService = lanDbJDBCService;
    }

    public Optional<ImpactData> getData(String userId) {
        LocalDateTime now = LocalDateTime.now();
        int participantId = Integer.parseInt(userId);
        String startDate = formatDate(now.minusDays(2));
        String endDate = formatDate(now.plusDays(2));
        String path = String.format("/activity?participantId=%d&&dateType=Scheduled Start&&startDate=%s&&endDate=%s", participantId, startDate, endDate);
        try {
            List<ActivityDTO> dto = impactQueryProxy.queryPagedResult(userId, path, new ParameterizedTypeReference<PagedList<ActivityDTO>>() {
            });
            List<ActivityDTO> fullActivities = pullAllData(filterPending(dto), userId);
            return Optional.of(new ImpactData(fullActivities));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public Optional<List<LocationAccess>> getLocationAccessForEquipment(String userId, String functionalPosition) {
        return lanDbJDBCService.getDeviceInfo(functionalPosition)
                .map(LanDbInfo::getLocation)
                .map(location -> getLocationAccess(userId, location)
                        .orElse(Collections.emptyList()));

    }

    public Optional<List<LocationAccess>> getLocationAccess(String userId, String location) {
        List<LocationAccess> locationAccess = impactLocationService.getLocationAccess(userId, location);
        if (location.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(locationAccess);
    }

    private List<OrgUnitDTO> getOrgUnitIds(String userId, String groupName) {
        String path = "orgUnit/?name=" + groupName;
        return impactQueryProxy.queryPagedResult(userId, path, new ParameterizedTypeReference<PagedList<OrgUnitDTO>>() {
        });
    }

    private List<ActivityDTO> pullAllData(List<ActivityDTO> result, String userId) {
        return result.parallelStream()
                .map(activity -> getActivityData(activity.getId(), userId))
                .collect(Collectors.toList());
    }

    private ActivityDTO getActivityData(long id, String userId) {
        String path = "activity/" + id;
        return impactQueryProxy.query(userId, path, ActivityDTO.class);
    }

    private List<ActivityDTO> filterPending(List<ActivityDTO> result) {
        return result.stream()
                .filter(this::isStillPending)
                .collect(Collectors.toList());
    }

    private boolean isStillPending(ActivityDTO activity) {
        List<String> finishedStates = Arrays.asList("Cancelled", "Closed", "Finished");
        return !finishedStates.contains(activity.getStatus().getLabel());
    }

    private String formatDate(LocalDateTime date) {
        DateTimeFormatter fm = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        fm.withZone(ZoneId.of("UTC"));
        return fm.format(date);
    }
}
