package ch.cern.be.ics.plcinfo.impact.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ActivityDTO {
    private long id;
    private String title;
    private String description;
    private StatusDTO status;
    private PersonDTO responsible;
    private SystemDTO system;
    private FacilityDTO facility;
    private Set<ApprovalDTO> approvals;
    private Set<ActivityLocationDTO> locations;
    private int maxParticipants;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLink() {
        return "https://impact.cern.ch/impact/secure/?place=editActivity:" + getId();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PersonDTO getResponsible() {
        return responsible;
    }

    public void setResponsible(PersonDTO responsible) {
        this.responsible = responsible;
    }

    public SystemDTO getSystem() {
        return system;
    }

    public void setSystem(SystemDTO system) {
        this.system = system;
    }

    public Set<ApprovalDTO> getApprovals() {
        return approvals;
    }

    public void setApprovals(Set<ApprovalDTO> approvals) {
        this.approvals = approvals;
    }

    public void setStatus(StatusDTO status) {
        this.status = status;
    }

    public void setFacility(FacilityDTO facility) {
        this.facility = facility;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public StatusDTO getStatus() {
        return status;
    }

    public FacilityDTO getFacility() {
        return facility;
    }

    public Set<ActivityLocationDTO> getLocations() {
        return locations;
    }

    public void setLocations(Set<ActivityLocationDTO> locations) {
        this.locations = locations;
    }
}
