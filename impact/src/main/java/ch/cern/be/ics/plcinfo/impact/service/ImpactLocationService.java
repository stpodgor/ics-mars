package ch.cern.be.ics.plcinfo.impact.service;

import ch.cern.be.ics.plcinfo.impact.dto.AccessPointsForLocation;
import ch.cern.be.ics.plcinfo.impact.dto.LocationAccess;
import ch.cern.be.ics.plcinfo.impact.input.LocationDTO;
import ch.cern.be.ics.plcinfo.impact.input.PagedList;
import org.springframework.core.ParameterizedTypeReference;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

@Named
public class ImpactLocationService {

    private final ImpactQueryProxy impactQueryProxy;

    @Inject
    public ImpactLocationService(ImpactQueryProxy impactQueryProxy) {
        this.impactQueryProxy = impactQueryProxy;
    }

    public List<LocationAccess> getLocationAccess(String userId, String locationName) {
        List<LocationDTO> matchingLocations = getMatchingLocations(userId, locationName);
        if (matchingLocations.isEmpty() && containsRoom(locationName)) {
            locationName = stripRoom(locationName);
            matchingLocations = getMatchingLocations(userId, locationName);
        }
        if (matchingLocations.isEmpty() && containsFloor(locationName)) {
            locationName = stripFloor(locationName);
            matchingLocations = getMatchingLocations(userId, locationName);
        }
        return getLocationAccess(userId, matchingLocations);
    }

    private boolean containsFloor(String locationName) {
        return locationName.contains("/");
    }

    private boolean containsRoom(String locationName) {
        return locationName.contains("-");
    }

    private String stripFloor(String locationName) {
        locationName = locationName.substring(0, locationName.indexOf("/"));
        return locationName;
    }

    private String stripRoom(String locationName) {
        locationName = locationName.substring(0, locationName.indexOf("-"));
        return locationName;
    }

    private List<LocationDTO> getMatchingLocations(String userId, String locationName) {
        String path = "location/?name=" + locationName;
        return impactQueryProxy.queryPagedResult(userId, path, new ParameterizedTypeReference<PagedList<LocationDTO>>() {
        });
    }

    private List<LocationAccess> getLocationAccess(String userId, List<LocationDTO> matchingLocations) {
        return matchingLocations
                .stream()
                .map(location -> new LocationAccess(location, getAccessPoints(userId, location)))
                .collect(Collectors.toList());
    }

    private AccessPointsForLocation getAccessPoints(String userId, LocationDTO location) {
        String path = "/accessPoint/bylocation/" + location.getId();
        return impactQueryProxy.query(userId, path, AccessPointsForLocation.class);
    }
}
