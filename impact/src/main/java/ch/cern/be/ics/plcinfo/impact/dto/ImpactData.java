package ch.cern.be.ics.plcinfo.impact.dto;

import ch.cern.be.ics.plcinfo.impact.input.ActivityDTO;

import java.util.Collections;
import java.util.List;

public class ImpactData {
    private final List<ActivityDTO> data;

    public ImpactData() {
        this(Collections.emptyList());
    }

    public ImpactData(List<ActivityDTO> data) {
        this.data = data;
    }

    public List<ActivityDTO> getData() {
        return data;
    }
}
