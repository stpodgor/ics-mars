package ch.cern.be.ics.plcinfo.impact.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
class ApprovalDTO {
    private ApprovalTypeDTO approvalType;
    private ApprovalStatusDTO approvalStatus;
    private String reason;
    private PersonDTO approver;
    private String approvalRole;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public PersonDTO getApprover() {
        return approver;
    }

    public void setApprover(PersonDTO approver) {
        this.approver = approver;
    }

    public String getApprovalRole() {
        return approvalRole;
    }

    public void setApprovalRole(String approvalRole) {
        this.approvalRole = approvalRole;
    }

    public ApprovalTypeDTO getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(ApprovalTypeDTO approvalType) {
        this.approvalType = approvalType;
    }

    public ApprovalStatusDTO getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatusDTO approvalStatus) {
        this.approvalStatus = approvalStatus;
    }
}
