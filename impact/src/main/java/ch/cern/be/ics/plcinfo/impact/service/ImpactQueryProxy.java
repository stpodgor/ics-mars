package ch.cern.be.ics.plcinfo.impact.service;

import ch.cern.be.ics.plcinfo.commons.Credentials;
import ch.cern.be.ics.plcinfo.impact.input.ActivityDTO;
import ch.cern.be.ics.plcinfo.impact.input.PagedList;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import javax.inject.Named;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

@Named
public class ImpactQueryProxy {
    private final String credentials = "Basic " + new String(Base64.getEncoder().encode((Credentials.login + ":" + Credentials.pass2).getBytes()));
    private final RestTemplate restTemplate = new RestTemplate();
    private final String basePath = "https://impact.cern.ch/impact/api/";

    public <T> T query(String userId, String path, Class<T> returnType) {
        ResponseEntity<T> response = restTemplate.exchange(basePath + path, HttpMethod.GET, prepareHeaders(userId), returnType);
        return response.getBody();
    }

    public <T> List<T> queryPagedResult(String userId, String path, ParameterizedTypeReference<PagedList<T>> parameterType) {
        ResponseEntity<PagedList<T>> response = restTemplate.exchange(basePath + path, HttpMethod.GET, prepareHeaders(userId), parameterType);
        return response.getBody().getResult();
    }

    private HttpEntity<PagedList<ActivityDTO>> prepareHeaders(String userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", credentials);
        headers.add("X-Ais-User-Id", userId);
        return new HttpEntity<>(headers);
    }
}
