package ch.cern.be.ics.plcinfo.dbabco.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.commons.service.BaseJdbcService;
import ch.cern.be.ics.plcinfo.commons.service.DbType;
import ch.cern.be.ics.plcinfo.commons.service.JdbcQueryProxy;
import ch.cern.be.ics.plcinfo.dbabco.dto.ScadaData;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Named
public class DBABCOService extends BaseJdbcService {

    @Inject
    DBABCOService(PLCInfoLogger logger, JdbcQueryProxy proxy) {
        super(DbType.ICESAS, logger, proxy);
    }

    public Optional<ScadaData> getData(String deviceFunctionalPosition) {
        try {
            PreparedStatement preparedStatement = getPreparedQuery();
            preparedStatement.setString(1, deviceFunctionalPosition);
            ResultSet results = preparedStatement.executeQuery();
            if (results.next()) {
                return Optional.of(new ScadaData(results.getString(1), results.getString(2), results.getString(3), results.getString(4), results.getString(5), results.getString(6), fixUTF8(stripNewlines(results.getString(7)))));
            }
        } catch (SQLException e) {
            logger.warning("Error when collecting data from ICESAS, " + e.getMessage());
            logger.warning(e);
            restartConnection();
        }
        return Optional.empty();
    }

    private String stripNewlines(String comments) {
        return Optional.ofNullable(comments)
                .map(c -> c.replaceAll("\n|\r|\r\n", " "))
                .orElse("");
    }

    /**
     * For whatever reason there are some hex escaped sequences stored as string...
     */
    private String fixUTF8(String comments) {
        Matcher m = Pattern.compile("(\\\\x[0-9a-fA-F][0-9a-fA-F])").matcher(comments);
        while (m.find()) {
            String hexEscapeSequence = m.group(1);
            try {
                String hexValue = hexEscapeSequence.replace("\\x", "");
                int value = Integer.parseInt(hexValue, 16);
                comments = comments.replace(hexEscapeSequence, String.valueOf((char) value));
            } catch (Exception ex) {
                comments = comments.replace(hexEscapeSequence, "");
            }
        }
        return comments;
    }

    @Override
    protected String getQuery() {
        return "select * from ICEDM_S_FW_SYS_STAT_DEVICES where upper(device_name) = ?";
    }
}
