package ch.cern.be.ics.plcinfo.dbabco.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.exception.DataCollectionException;
import ch.cern.be.ics.plcinfo.dbabco.dto.ScadaData;
import ch.cern.be.ics.plcinfo.dbabco.service.DBABCOService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;

@RestController
@RequestMapping("/api/icesas")
public class IcesasRestController {

    private final DBABCOService dbabcoService;

    @Inject
    public IcesasRestController(DBABCOService dbabcoService) {
        this.dbabcoService = dbabcoService;
    }

    @CrossOrigin
    @RequestMapping(value = "/general", method = RequestMethod.GET)
    public ResponseEntity<ScadaData> getIcesasData(@RequestParam String[] code) throws DataCollectionException {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(dbabcoService::getData, Arrays.stream(code)));
    }
}
