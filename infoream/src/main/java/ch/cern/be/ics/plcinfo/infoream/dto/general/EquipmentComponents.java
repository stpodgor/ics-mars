package ch.cern.be.ics.plcinfo.infoream.dto.general;

import ch.cern.cmms.infor.wshub.Graph;
import ch.cern.cmms.infor.wshub.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EquipmentComponents {
    private final List<EquipmentComponent> components = new ArrayList<>();

    public EquipmentComponents(Map<Node, Graph> childrenEquipmentGraph) {
        components.addAll(childrenEquipmentGraph.entrySet()
                .stream()
                .map(subChildrenGraphEntry -> new EquipmentComponent(subChildrenGraphEntry.getKey(), subChildrenGraphEntry.getValue()))
                .collect(Collectors.toList()));
    }

    public List<EquipmentComponent> getComponents() {
        return components;
    }
}
