package ch.cern.be.ics.plcinfo.infoream.dto.workorder;

import java.util.List;

public class WorkOrderData {
    private final String status;
    private final String workOrderId;
    private final String equipmentName;
    private final String description;
    private final String department;
    private final String orderClass;
    private final String reportedBy;
    private final String scheduledStart;


    public WorkOrderData(List<String> data) {
        workOrderId = data.get(5);
        description = data.get(6);
        status = data.get(7);
        department = data.get(9);
        equipmentName = data.get(10);
        orderClass = data.get(11);
        reportedBy = data.get(12);
        scheduledStart = data.get(13);
    }

    public String getStatus() {
        return status;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public String getDescription() {
        return description;
    }

    public String getDepartment() {
        return department;
    }

    public String getOrderClass() {
        return orderClass;
    }

    public String getReportedBy() {
        return reportedBy;
    }

    public String getScheduledStart() {
        return scheduledStart;
    }
}
