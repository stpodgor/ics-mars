package ch.cern.be.ics.plcinfo.infoream.dto.workorder;

import java.util.List;

public class WorkOrdersList {
    private final List<WorkOrderData> workOrders;

    public WorkOrdersList(List<WorkOrderData> data) {
        workOrders = data;
    }

    public List<WorkOrderData> getWorkOrders() {
        return workOrders;
    }
}
