package ch.cern.be.ics.plcinfo.infoream.dto.general;

import ch.cern.be.ics.plcinfo.infoream.service.HierarchyUtils;
import ch.cern.cmms.infor.wshub.Graph;
import ch.cern.cmms.infor.wshub.Node;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EquipmentComponent {
    private final String name;
    private final String description;
    private final List<EquipmentComponent> children;

    EquipmentComponent(Node node, Graph childrenGraph) {
        this.name = node.getCode();
        this.description = node.getDesc();
        Map<String, Node> childNodesById = HierarchyUtils.getChildNodesById(childrenGraph);
        children = getChildrenNodes(childrenGraph, childNodesById);
    }

    private List<EquipmentComponent> getChildrenNodes(Graph childrenGraph, Map<String, Node> childNodesById) {
        return HierarchyUtils.getChildrenNodes(childrenGraph, this.name, childNodesById)
                .map(child -> new EquipmentComponent(child, new Graph()))
                .collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<EquipmentComponent> getChildren() {
        return children;
    }
}
