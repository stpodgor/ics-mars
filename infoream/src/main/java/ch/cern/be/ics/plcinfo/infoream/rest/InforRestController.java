package ch.cern.be.ics.plcinfo.infoream.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.exception.DataCollectionException;
import ch.cern.be.ics.plcinfo.infoream.dto.general.InforEamInfo;
import ch.cern.be.ics.plcinfo.infoream.dto.workorder.WorkOrdersList;
import ch.cern.be.ics.plcinfo.infoream.service.InforEamService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;

@RestController
@RequestMapping("/api/infor")
public class InforRestController {
    private final InforEamService inforEamService;

    @Inject
    public InforRestController(InforEamService inforEamService) {
        this.inforEamService = inforEamService;
    }

    @CrossOrigin
    @RequestMapping(value = "/position", method = RequestMethod.GET)
    public ResponseEntity<InforEamInfo> getInforDataForFunctionalPosition(@RequestParam String[] code) throws DataCollectionException {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(inforEamService::getDeviceInfoFromFunctionalPosition, Arrays.stream(code)));
    }

    @CrossOrigin
    @RequestMapping(value = "/workOrders", method = RequestMethod.GET)
    public ResponseEntity<WorkOrdersList> getWorkOrders(@RequestParam String[] code) throws DataCollectionException {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(inforEamService::getWorkOrdersList, Arrays.stream(code)));
    }
}
