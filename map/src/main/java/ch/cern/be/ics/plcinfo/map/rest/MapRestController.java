package ch.cern.be.ics.plcinfo.map.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.exception.DataCollectionException;
import ch.cern.be.ics.plcinfo.map.service.MapService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Arrays;

@RestController
@RequestMapping("/api/map")
public class MapRestController {

    private final MapService mapService;

    @Inject
    public MapRestController(MapService mapService) {
        this.mapService = mapService;
    }


    @CrossOrigin
    @RequestMapping(value = "/location", method = RequestMethod.GET)
    public ResponseEntity<String> getMapLink(@RequestParam String[] code) throws DataCollectionException {
        return Utils.formResponse(Utils.tryDataUntilNonEmptyResult(mapService::getMapLink, Arrays.stream(code)));
    }
}
