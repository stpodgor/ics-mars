package ch.cern.be.ics.plcinfo.identifier.dto;

import java.util.List;

public class EquipmentIdentifier {
    private final List<String> potentialIdentifiers;

    public EquipmentIdentifier(List<String> potentialIdentifiers) {
        this.potentialIdentifiers = potentialIdentifiers;
    }

    public List<String> getPotentialIdentifiers() {
        return potentialIdentifiers;
    }
}
