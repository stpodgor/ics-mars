package ch.cern.be.ics.plcinfo.userinfo.service;

import ch.cern.be.ics.plcinfo.commons.PLCInfoLogger;
import ch.cern.be.ics.plcinfo.commons.service.BaseJdbcService;
import ch.cern.be.ics.plcinfo.commons.service.DbType;
import ch.cern.be.ics.plcinfo.commons.service.JdbcQueryProxy;
import ch.cern.be.ics.plcinfo.userinfo.dto.UserInfo;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Named
public class UserInfoJdbcService extends BaseJdbcService {

    @Inject
    protected UserInfoJdbcService(PLCInfoLogger logger, JdbcQueryProxy proxy) {
        super(DbType.CERN_DB1, logger, proxy);
    }

    public Optional<UserInfo> getUserInfo(String surname) {
        try {
            PreparedStatement preparedStatement = getPreparedQuery();
            preparedStatement.setString(1, surname.toUpperCase());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(new UserInfo(resultSet.getString(1)));
            }
        } catch (SQLException e) {
            logger.warning("Error when collecting user info, " + e.getMessage());
            logger.warning(e);
            restartConnection();
        }
        return Optional.empty();
    }


    @Override
    protected String getQuery() {
        return "select pi.PERSON_ID from landb.PERSONAL_INFORMATION_PUBLIC pi where pi.SURNAME = ?";
    }
}
