package ch.cern.be.ics.plcinfo.userinfo.rest;

import ch.cern.be.ics.plcinfo.commons.Utils;
import ch.cern.be.ics.plcinfo.commons.exception.DataCollectionException;
import ch.cern.be.ics.plcinfo.userinfo.dto.SSOBean;
import ch.cern.be.ics.plcinfo.userinfo.dto.UserInfo;
import ch.cern.be.ics.plcinfo.userinfo.filter.SSOFilter;
import ch.cern.be.ics.plcinfo.userinfo.service.UserInfoJdbcService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/users")
public class UserdataRestController {

    private final UserInfoJdbcService userInfoJdbcService;
    private final SSOFilter ssoFilter;

    @Inject
    public UserdataRestController(UserInfoJdbcService userInfoJdbcService, SSOFilter ssoFilter) {
        this.userInfoJdbcService = userInfoJdbcService;
        this.ssoFilter = ssoFilter;
    }

    @CrossOrigin
    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public ResponseEntity<UserInfo> getUserInfo(@RequestParam String surname) throws DataCollectionException {
        return Utils.formResponse(userInfoJdbcService.getUserInfo(surname));
    }

    @CrossOrigin
    @RequestMapping(value = "/sso", method = RequestMethod.GET)
    public ResponseEntity<SSOBean> getLoggedUser() throws DataCollectionException {
        return Utils.formResponse(ssoFilter.getBean());
    }
}
