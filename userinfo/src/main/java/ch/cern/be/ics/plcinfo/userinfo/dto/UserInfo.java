package ch.cern.be.ics.plcinfo.userinfo.dto;

public class UserInfo {
    private final String id;

    public UserInfo(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
