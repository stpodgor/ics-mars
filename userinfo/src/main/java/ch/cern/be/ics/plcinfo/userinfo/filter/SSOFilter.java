package ch.cern.be.ics.plcinfo.userinfo.filter;

import ch.cern.be.ics.plcinfo.userinfo.dto.SSOBean;

import javax.inject.Named;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@Named
public class SSOFilter implements Filter {

    private String userId;
    private String userLogin;
    private String userName;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        userId = getHeaderParameter((HttpServletRequest) req, "ADFS_PERSONID", "726848");
        userLogin = getHeaderParameter((HttpServletRequest) req, "ADFS_LOGIN");
        userName = getHeaderParameter((HttpServletRequest) req, "ADFS_FULLNAME");
        chain.doFilter(req, res);
    }

    private String getHeaderParameter(HttpServletRequest req, String name) {
        return Optional.ofNullable(req.getHeader(name))
                .orElse("unknown");
    }

    private String getHeaderParameter(HttpServletRequest req, String name, String defaultValue) {
        return Optional.ofNullable(req.getHeader(name))
                .orElse(defaultValue);
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }

    public String getUserId() {
        return userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public String getUserName() {
        return userName;
    }

    public Optional<SSOBean> getBean() {
        return Optional.of(new SSOBean(getUserId(), getUserLogin(), getUserName()));
    }
}
